# Terraformインストール（Windows）

インストール手順を２つ紹介いたします。いずれか実行いただければ問題ありません。

- [Terraformインストール（Windows）](#terraformインストールwindows)
  - [バイナリを用いる場合](#バイナリを用いる場合)
  - [tfenvを用いる場合](#tfenvを用いる場合)

versionは2020/12/10現在のEponaが利用するバージョンv0.13.5を利用します。

## バイナリを用いる場合

[こちら](https://releases.hashicorp.com/terraform/0.13.5/)から、Terraformのバイナリ(zip圧縮)をダウンロードしてください。

zipファイルを解凍し、terraformファイルを任意のディレクトリにコピーします。

環境変数の設定から、上記ディレクトリにパスを通してください。

以下が確認できればOKです。

```shell script
$ terraform --version
Terraform v0.13.5
```

## tfenvを用いる場合

tfenvを利用するとTerraformのバージョン変更が楽になります。

[tfenv installation](https://github.com/tfutils/tfenv#manual)

tfenvがインストールされたことを確認します。

```shell script
$ tfenv -v
```

tfenvを使ってTerraformをインストールします。

```shell script
$ tfenv install 0.13.5
```

どのバージョンを利用するか指定します。

```shell script
$ tfenv use 0.13.5
```

以下が確認できればOKです。

```shell script
$ terraform --version
Terraform v0.13.5
```
